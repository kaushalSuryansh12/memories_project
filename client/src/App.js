import './App.css';
import React, { useEffect, useState } from 'react';
import { Container, AppBar, Typography, Grow, Grid } from '@material-ui/core';

import { getPosts} from './actions/posts'; 
import memories from './images/memories.png';
import Posts from './components/Posts/Posts';
import Form from './components/Form/Form';
import useStyles from './styles';
import { useDispatch } from 'react-redux';

const App = () => {
     
     const [currentId, setCurrentId] = useState(0);
     const classes = useStyles();
     const dispatch = useDispatch();

    // useEffect having currentId will reload the page everytime the currentId changes
    // as we are clearing the input and clearing the input means changing the id
    // and as soon as we change the id the app is gonna dispatch the getpost action

     useEffect (() => {
        dispatch(getPosts());
     }, [currentId, dispatch]);

    return (
        <Container maxwidth="lg">
          <AppBar className = {classes.appBar} position="static" color="inherit">
            <Typography className = {classes.heading} variant="h3" align="center">Memories</Typography>
            <img className = {classes.image} src={memories} alt="memories" height="80" width="80" />
          </AppBar>
          {/* Grow will have a property in */}
          <Grow in>
            {/* Grid component of type container */}
            <Container>
              <Grid className={classes.mainContainer} container justify="space-between" alignItems="stretch" spacing={3} >
                <Grid item xs={12} sm={7}>
                  <Posts setCurrentId={setCurrentId} />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <Form currentId={currentId} setCurrentId={setCurrentId} />
                </Grid>
              </Grid>
            </Container>
          </Grow>
        </Container>
    );

}



export default App;
