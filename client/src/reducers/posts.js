// a reducer is a funciton that accepts the state and also it accepts the action 
// and the based on the action type than you can do some logic

// renamed states to posts

export default (posts = [], action) => {
    switch(action.type){
        case 'FETCH_ALL':
            console.log('Payload', action.payload);
            return action.payload;
        case 'CREATE':
            // we have to send over an array of posts, and then we have to add a new post, and the new post is stored in action.payload
            return [...posts, action.payload];
        case 'UPDATE': 
        case 'LIKE':
            // keeping in mind that action.paylod is the updated post
            return posts.map((post) => ( post._id === action.payload._id ? action.payload : post ));
        case 'DELETE':
            return posts.filter((post) => post._id !== action.payload);
        default:
            return posts;
    }
}