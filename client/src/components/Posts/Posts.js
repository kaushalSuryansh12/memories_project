import React from 'react';
import { Grid, CircularProgress } from '@material-ui/core';
import Post from './Post/Post';
import useStyles from './styles';
// we have to fetch data from the global redux store we can do it with the help of selector mention under
import { useSelector } from 'react-redux';


const Posts = ({setCurrentId }) => {
    // go to reducer you wil see its called posts
    // initialize as a hook. Inside the hook we have a callback function we get access to the whole store
    // how do we know it is called post its in the reducer
    //fetching all the post
    const posts = useSelector((state) => state.posts);
    const classes = useStyles(); 

    return (
        !posts.length ? <CircularProgress /> : (
            <Grid className = {classes.container} container alignItems="strech" spacing={3}>
                {posts.map((post) => (

                    <Grid key={post._id} item xs={12} sm={6}>
                        <Post post={post} setCurrentId={setCurrentId} />
                    </Grid>

                    ))}
            </Grid>
        )
    );
}

export default Posts;