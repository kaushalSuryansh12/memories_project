import React, {useState, useEffect} from 'react';
import { TextField, Button, Typography, Paper } from '@material-ui/core';
import useStyles from './styles';
import FileBase from 'react-file-base64';
import { useDispatch, useSelector } from 'react-redux';
import { createPost, updatePost } from '../../actions/posts'; 

//Get the current id of the post

const Form = ({currentId, setCurrentId}) => {
    const [postData, setPostData] = useState({creator: '', title: '', message: '', tags: '', selectedFile: ''});
    //fetching the post but only the updated one
    const post = useSelector((state) => (currentId ? state.posts.find((p) => p._id === currentId) : null));
    const dispatch = useDispatch();
    const classes = useStyles();

    //useeffect uses two parameters first one being a callback function and second one being a dependency array
    // in the array it says when should the post back function should run well when the post changes from null to something

    useEffect(() => {

     if(post) setPostData(post);

    }, [post]);

    const handleSubmit = (e) => {
        e.preventDefault(); // not to get refresh in the browser
        //pass the data to createPost from the state

        console.log('****FORM SUBMITTED****');

        if(currentId === 0) {
            dispatch(createPost(postData));       
        } else {
            dispatch(updatePost(currentId, postData));
            console.log('DATA TO BE FOUND', currentId, postData); 
        }

        clear();

    }

    const clear = () => {

        setCurrentId(null);
        setPostData({creator: '', title: '', message: '', tags: '', selectedFile: ''});

    }

//postData is a object state that means the whole data in the post will be stored in this object
    return (
        <Paper className = {classes.paper}>
            <form autoComplete="off" noValidate className = {`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
                <Typography variant="h6">
                    {currentId ? 'Editing' : 'Creating' } a Memory
                </Typography>
                <TextField name="creator" variant="outlined" label="Creator" fullWidth value={postData.creator} onChange={(e) => setPostData({...postData, creator: e.target.value})} />
                <TextField name="title" variant="outlined" label="Title" fullWidth value={postData.title} onChange={(e) => setPostData({...postData, title: e.target.value})} />
                <TextField name="message" variant="outlined" label="Message" fullWidth value={postData.message} onChange={(e) => setPostData({...postData, message: e.target.value})} />
                <TextField name="tags" variant="outlined" label="Tags" fullWidth value={postData.tags} onChange={(e) => setPostData({...postData, tags: e.target.value.split(',')})} />
                <div className = {classes.fileInput}><FileBase name="selectedFile" type="file" multiple={false} onDone={({base64}) => setPostData({...postData, selectedFile: base64})} /></div>
                <Button className = {classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>Submit</Button>
                <Button variant="contained" color="secondary" size="small" onClick={clear} fullWidth>Clear</Button>

            </form>
        </Paper>
    );
}

export default Form;