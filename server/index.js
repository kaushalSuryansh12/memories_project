import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import postRoutes from './routes/posts.js';

const app = express(); 
dotenv.config();

app.use(cors());

//the properties in the object is used as the project is using big imgages
app.use(bodyParser.json({limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({limit: "30mb", extended: true }));

//always specify routes after cors and bodyParser
app.use('/posts', postRoutes);

const PORT = process.env.PORT || 5000;

// the object in the connect is used to avoid warning and errors
mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
        .catch((err) => console.log(err.message));

//this makes sure we don't get any warning in the console
//mongoose.set('useFindAndModify', false);