import express from 'express';
//in react we dont need to add .js at the end of the file 
// but in node we need to do
import { getPosts, createPost, updatePost, deletePost, likePost } from '../controllers/posts.js';

const router = express.Router();

//we added a prefix to routes
// http://localhost:5000/posts

router.get('/', getPosts);

router.post('/', createPost);
//patch is used for updating existing documents paritally
router.patch('/:id', updatePost);

router.delete('/:id', deletePost);

router.patch('/:id/likePost', likePost);

export default router;